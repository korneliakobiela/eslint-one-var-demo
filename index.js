const path = require('path');
const fs = require('fs');

fs.readFile(path.join(__dirname, 'package.json'), 'utf8', (err, data) => {
    if (err) {
        throw err;
    }
    const welcome = 'Hello';
    const devDependencies = JSON.parse(data).devDependencies;

    for (const dependency in devDependencies.devDependencies) {
        if (devDependencies.hasOwnProperty(dependency)) {
            const element = devDependencies[dependency];
            /* eslint-disable no-alert, no-console */
            console.log(`${welcome}, ${element}!`);
        }
    }
    
});